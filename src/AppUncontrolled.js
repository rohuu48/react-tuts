import React, { Component } from 'react';

class AppUncontrolled extends Component {
    constructor(props){
        super(props);
        this.input = React.createRef();
    }
    
    handleChange = (newText) => {
        console.log(newText);
    }
    render() {
        return (
            <div className="App2">
                <div className="container">
                    <input type="text"
                        placeholder="Your message here.."
                        ref={this.input}
                        onChange={(event) => this.handleChange(event.target.value)}
                    />
                </div>
            </div>
            
        );
    }
}
export default AppUncontrolled;

// form data is handled by the DOM itself. To write an uncontrolled component, instead of writing an event handler for every state update, you can use a ref to get form values from the DOM.